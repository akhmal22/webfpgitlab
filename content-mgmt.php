<?php 
    include 'cms-header.php';
    
    /*
    $cadmintable = "CREATE TABLE admin_data(ademail VARCHAR(320) NOT NULL, password VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, adaddress VARCHAR(255) NOT NULL, PRIMARY KEY(email));";
    if($conn->query($cadmintable))
    {
        echo "worked";
    }else{
        die("not worked");
    }
    */
?>

<!DOCTYPE html>
<html>
    <head>
        <title>content management testing</title>
    </head>
    <body>
        <h1>What do you wanna do?</h1>
        <ul>
            <form action="" method="post">
                <input type="submit" name="insertprod" value="INSERT PRODUCT">
            </form>
            <?php
                
                if(isset($_POST['insertprod'])):
            ?>
                    <p>Which product you wished to be inserted?</p>
                    <form action="cms-backend.php" method="post">
                        <input type="text" name="productname" placeholder="product name">
                        <input type="text" name="productprice" placeholder="price">
                        <input type="text" name="prodcat" placeholder="categories">
                        <input type="submit" name="executeins" value="execute">
                    </form>
            <?php 
                endif;
            ?>

            <form action="" method="post">
                <input type="submit" name="updateprod" value="UPDATE PRODUCT">
            </form>
            <?php 
                if(isset($_POST['updateprod'])):
            ?>
                    <p>Which product you wished to be updated?</p>
                    <form action="cms-backend.php" method="post">
                        <input type="text" name="productname" placeholder="product name">
                        <input type="text" name="productprice" placeholder="price">
                        <input type="text" name="prodcat" placeholder="categories">
                        <input type="text" name="prodid" placeholder="product id">
                        <input type="submit" name="executeupd" value="execute">
                    </form>
            <?php 
                endif;
            ?>

            
            <form action="" method="post">
                <input type="submit" name="deleteprod" value="DELETE PRODUCT">
                
            </form>
            <?php 
                if(isset($_POST['deleteprod'])):
            ?>
                    <p>Which product you wished to be deleted?</p>
                    <form action="cms-backend.php" method="post">
                        <input type="text" name="prodid" placeholder="product id">
                        <input type="submit" name="executedel" value="execute">
                    </form>
            <?php 
                endif;
            ?>
            <form action="" method="post">
                <input type="submit" name="readprod" value="READ PRODUCT">
            </form>
            <?php 
                if(isset($_POST['readprod'])):
            ?>
                    <p>View all product?</p>
                    <form action="cms-backend.php" method="post">                        
                        <input type="submit" name="executeread" value="execute">
                    </form>
            <?php 
                endif;
            ?>
        </ul>
    </body>
</html>