<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>HAOZ-HAOZ | Admin Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <link rel="stylesheet" href="../plugins/iCheck/background.css">


  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php 
  include "../db-connect.php";
  if(isset($_POST['registerbutton'])){
		//echo "pressed";
		if($_POST['name'] && $_POST['email'] && $_POST['pass'] && $_POST['confirmpass']):
		  $name = $_POST['name'];
			$email = $_POST['email'];
			$password = $_POST['pass'];
			$confpass = $_POST['confirmpass'];
			$insertadm = "INSERT INTO admin_data (name,email,password) VALUES ('$name','$email','$password');";
			if($password==$confpass){
				if($conn->query($insertadm)){
          header("Location: loginAdmin.php");
        }else{
          echo "cannot insert";
        }
			}else{
				echo "password not matched";
			}
			
		else:
			echo "Fill the blanks";
		endif;
		
	}
?>
<body class="hold-transition login-page">
  <div class="background-image"></div>
    <div class="content">
      <div class="login-box">
        <div class="login-logo">
          <img style="width:150px; height: 120px" src="../images/icons/logo.png">
        </div>
        <!-- /.login-logo -->
          <div class="login-box-body">
            <p class="login-box-msg" >Register Your Admin Account Haoz-Haoz!</p>

            <form action="" method="post">
              <div class="form-group has-feedback">
                <input type="name" name="name" class="form-control" placeholder="Fullname"> 
              </div>
              <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="Email">
              </div>
              <div class="form-group has-feedback">
                <input type="password" name="pass" class="form-control" placeholder="Password">
              </div>
              <div class="form-group has-feedback">
                <input type="password" name="confirmpass" class="form-control" placeholder="Confirm Password">
              </div>
              <div class="row">
                 <button type="submit" name="registerbutton" value="REGISTER" class="btn btn-success btn-block btn-flat">Register</button>
              </div>
            </form>
          </div>
  <!-- /.login-box-body -->
</div>
</div>

<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
