<?php 
  ob_start();
  session_start();
  include '../db-connect.php';
?>
<?php 
    include '../cms-header.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>HAOZ-HAOZ | Admin Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <link rel="stylesheet" href="../plugins/iCheck/background.css">


  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php 
  include "../db-connect.php";
  if(isset($_POST['signinbutt'])){
		if($_POST['emailadm'] && $_POST['passwordadm']){
			/*
			$sessionValid = $_SESSION['valid'];
			$sessionTimeout = $_SESSION['timeout'];
			$sessionEmail = $_SESSION['email'];
			 */
			$email = $_POST['emailadm'];
			$passwd = $_POST['passwordadm'];
			$selectemail = "SELECT * FROM admin_data WHERE email = '$email';";
			if($conn->query($selectemail)->num_rows > 0){
				$row = $conn->query($selectemail)->fetch_assoc();
				if($passwd == $row["password"]){
					$_SESSION['adminvalid'] = true;
					$_SESSION['admintimeout'] = time();
					$_SESSION['adminemail'] = $email;
					header('Location: addMenu.php');
        }else if($_SESSION['adminvalid'] == true){
          echo "you are logged in";
        }else{
					echo "Password incorrect";
				}
				/*
				if($conn->num_rows($conn->query($selectemail)) > 0){
					echo "oke";
				}
				*/
			}else{
				//echo $conn->error();
				echo "Register your email first!";
			}



		}else{
			echo "fill the blanks";
		}
	}
?>
<body class="hold-transition login-page">
  <div class="background-image"></div>
  <div class="content">
<div class="login-box">
  <div class="login-logo">

    <img style="width:150px; height: 120px" src="../images/icons/logo.png">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg" >Welcome to Admin Page Haoz-Haoz</p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="email" name="emailadm" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="passwordadm" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Ingat Saya
            </label>
          </div>        
        </div>
        <!-- /.col -->      
        <div class="col-xs-4">
          <button type="submit" name="signinbutt" class="btn btn-warning btn-block btn-flat">Login</button>
        </div>      
      <!-- /.col -->
      </div>
    </form>
    
    

    <a href="registerAdmin.php" style="color: blue" class="text-center">Register Your Account Now!</a>


  </div>
  <!-- /.login-box-body -->
</div>
</div>

<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
