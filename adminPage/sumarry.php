<?php require_once('../include/adminHeader.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Menu 
        <small>Minuman</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <!-- CCTV -->
          <div class="box box-primary" style="width: 175%">
            <div class="box-body">
            
              <div class="container-fluid">
                  <!-- start: Content -->
                <div id="content" class="span10">
                        
                <div class="row-fluid sortable">
                  
                      <div class="row">
                        <div class="column" style="float: left; width: 33.33%; padding: 5px;">
                          <img src="../img/gallery/1.png" alt="Snow" style="width:100%">
                        </div>
                        <div class="column"  style="float: left; width: 33.33%; padding: 5px;">
                          <img src="../img/gallery/2.png" alt="Forest" style="width:100%">
                        </div>
                        <div class="column"  style="float: left; width: 33.33%; padding: 5px;">
                          <img src="../img/gallery/3.png" alt="Forest" style="width:100%">
                        </div>
                        <div class="column"  style="float: left; width: 33.33%; padding: 5px;">
                          <img src="../img/gallery/4.png" alt="Forest" style="width:100%">
                        </div>
                        <div class="column"  style="float: left; width: 33.33%; padding: 5px;">
                          <img src="../img/gallery/5.png" alt="Forest" style="width:100%">
                        </div>
                        <div class="column"  style="float: left; width: 33.33%; padding: 5px;">
                          <img src="../img/gallery/6.png" alt="Forest" style="width:100%">
                        </div>
                      </div>


                    <div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-content">
                        <ul class="list-inline item-details">
                          <li><a href="http://themifycloud.com">Admin templates</a></li>
                          <li><a href="http://themescloud.org">Bootstrap themes</a></li>
                        </ul>
                      </div>
                  </div><!--/span-->
                
                </div><!--/row-->
              

            </div><!--/.fluid-container-->
            
                <!-- end: Content -->
              </div><!--/#content.span10-->
              </div><!--/fluid-row-->
              
            </div>
          </div>
          <!-- /.box -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">


        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php require_once('../include/adminFooter.php'); ?>
