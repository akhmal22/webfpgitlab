<?php require_once('../include/adminHeader.php'); ?>
<?php 
  $selfood = "SELECT * FROM product_data WHERE categories = 'foods';";
  $qres = $conn->query($selfood);
  $numrowfood = $qres->num_rows;  
?>
<?php 
  $seldrink = "SELECT * FROM product_data WHERE categories = 'drinks';";
  $dres = $conn->query($seldrink);
  $numrowfood = $dres->num_rows;  
?>

<?php
  if(!isset($_SESSION['adminemail'])):
    header("Location: loginAdmin.php");
  else:
?>

    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          Menu 
          <small>All Products</small>
        </h1>
      </section>


    <?php   
      if ($numrowfood == 0):
        ?>
        <h4 class="m-text14 p-b-7">
          No Results
          
        </h4>
    <?php
      else:
    ?>

      <section class="content">

        <div class="row">
              <?php
                while($namerow = $qres->fetch_assoc()):
              ?>
                  
                  <div class="col-md-3">
                    <div class="box box-primary">
                          <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" src="../images/<?php echo $namerow['prodImage'];?>" alt="User profile picture">
                              <h3 class="profile-username text-center">
                                  <?php 
                                      echo $namerow['prodName'];
                                    ?>
                              </h3>
                          
                                <ul class="list-group list-group-unbordered">
                                  <li class=`"list-group-item">
                                    <b>ID items</b> <a class="pull-right">
                                      <?php 
                                        echo $namerow['prodId'];
                                      ?></a>
                                  </li>
                                </ul>
                                
                                <ul class="list-group list-group-unbordered">
                                  <li class=`"list-group-item">
                                    <b>Price</b> <a class="pull-right">
                                      <?php 
                                        echo $namerow['price'];
                                      ?></a>
                                  </li>
                                </ul>

                                <ul class="list-group list-group-unbordered">
                                  <li class=`"list-group-item">
                                    <b>Categories</b> <a class="pull-right">
                                      <?php 
                                        echo $namerow['categories'];
                                      ?></a>
                                  </li>
                                </ul>
                          </div>
                        </div>
                  </div>

                <?php
                  endwhile;
                ?>
            
            <?php
                while($namerow = $dres->fetch_assoc()):
              ?>
                  
                  <div class="col-md-3">
                    <div class="box box-primary">
                          <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" src="../images/<?php echo $namerow['prodImage'];?>" alt="User profile picture">
                              <h3 class="profile-username text-center">
                                  <?php 
                                      echo $namerow['prodName'];
                                    ?>
                              </h3>
                            
                            <ul class="list-group list-group-unbordered">
                              <li class=`"list-group-item">
                                <b>ID items</b> <a class="pull-right">
                                  <?php 
                                    echo $namerow['prodId'];
                                  ?></a>
                              </li>
                            </ul>

                            <ul class="list-group list-group-unbordered">
                              <li class=`"list-group-item">
                                <b>Price</b> <a class="pull-right">
                                  <?php 
                                    echo $namerow['price'];
                                  ?></a>
                              </li>
                            </ul>

                            <ul class="list-group list-group-unbordered">
                              <li class=`"list-group-item">
                                <b>Categories</b> <a class="pull-right">
                                  <?php 
                                    echo $namerow['categories'];
                                  ?></a>
                              </li>
                            </ul>
                          </div>
                        </div>
                  </div>

                <?php
                  endwhile;
                ?>


          </div>  
        </section>
    <?php
        endif;
      ?>
      
    </div>
<?php 
  endif;
?>
<div class="control-sidebar-bg"></div>
</div>

<?php require_once('../include/adminFooter.php'); ?>

