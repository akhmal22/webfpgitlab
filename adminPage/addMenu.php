<?php require_once('../include/adminHeader.php'); ?>
<?php
  include 'cms-header.php';
  if(!isset($_SESSION['adminemail'])):
    header("Location: loginAdmin.php");
  else:
?>
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Menu 
      </h1>
    </section>

      <div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab"><b>INSERT NEW MENU</b></a></li>
              <li><a href="#tab_2" data-toggle="tab"><b>UPDATE MENU</b></a></li>
              <li><a href="#tab_3" data-toggle="tab"><b>DELETE MENU</b></a></li>
            </ul>
            <div class="tab-content">
             

              <div class="tab-pane active" id="tab_1">
                <p>Which product you wished to be inserted?</p>
                <form action="" method="post">
                    <input type="text" class="form-control" name="productname" placeholder="Product Name">
                    <br>
                    <input type="text" class="form-control" name="productprice" placeholder="Price">
                    <br>
                    <input type="text" class="form-control" name="prodcat" placeholder="Categories">
                    <br>
                    <input type="text" class="form-control" name="prodimg" placeholder="Photo File Name">
                    <br>
                    <input type="submit" class="btn btn-success" name="executeins" value="Execute">
                </form>             
              </div>

              <div class="tab-pane" id="tab_2">
                <p>Which product you wished to be updated?</p>
                <form action="" method="post">
                    <input type="text" class="form-control" name="productnameupd" placeholder="New Product Name">
                    <br>
                    <input type="text" class="form-control" name="productpriceupd" placeholder="New Price">
                    <br>
                    <input type="text" class="form-control" name="prodcatupd" placeholder="Categories">
                    <br>
                    <input type="text" class="form-control" name="prodidupd" placeholder="Product ID">
                    <br>
                    <input type="text" class="form-control" name="prodimgupd" placeholder="img location">
                    <br>
                    <input type="submit" class="btn btn-success" name="executeupd" value="Execute">
                </form>
              </div>

              <div class="tab-pane" id="tab_3"> 
                <p>Which product you wished to be deleted?</p>
                <form action="" method="post">
                    <input type="text" class="form-control" name="prodid" placeholder="Product ID">
                    <br>
                    <input type="submit" class="btn btn-success" name="executedel" value="Execute">
                </form>
              </div>

            </div>
          </div>
        </div>
    </div>

    </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<?php

  endif;
    if(isset($_POST['executeins'])){
        if(!isset($_POST['productname']) || !isset($_POST['productprice']) || !isset($_POST['prodcat']) || !isset($_POST['prodimg'])){
            echo "jangan ada null di antara kita";
        }else{
            $postprodname = $_POST['productname'];
            $postprodprice = $_POST['productprice'];
            $postprodcat = $_POST['prodcat'];
            $postprodimg = $_POST['prodimg'];
            echo "executed";
            insertintoproduct($postprodname,$postprodprice,$postprodcat,$postprodimg);
        }
             
    }
    if(isset($_POST['executeupd'])){
      if(!isset($_POST['prodidupd']) || !isset($_POST['productnameupd']) || !isset($_POST['productpriceupd']) || !isset($_POST['prodcatupd']) || !isset($_POST['prodimgupd'])){
            echo "jangan ada null di antara kita";
        }else{
            $postprodname = $_POST['productnameupd'];
            $postprodprice = $_POST['productpriceupd'];
            $postprodcat = $_POST['prodcatupd'];
            $postprodimg = $_POST['prodimgupd'];
            $postprodid = $_POST['prodidupd'];
            echo "executedupd";
            echo $postprodimg;
            updateProduct($postprodname,$postprodprice,$postprodcat,$postprodid,$postprodimg);            
        }
    }
    if(isset($_POST['executedel'])){
        if(!isset($_POST['prodid'])){
            echo "jangan ada null di antara kita";
        }else{
            $postprodid = $_POST['prodid'];
            echo $postprodid;
            echo "executed";
            deleteProduct($postprodid);
        }
    }else if(isset($_POST['executeread'])){
        readProduct();
    }
?>
<?php require_once('../include/adminFooter.php'); ?>
