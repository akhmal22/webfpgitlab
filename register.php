<!DOCTYPE html>
<?php require_once('include/header.php'); ?>

	</header>

	<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/heading-pages-021.jpg);">
		<h2 class="l-text2 t-center">
			Register
		</h2>
	</section>
<?php 

	//$insertcred = "INSERT INTO credent (password) VALUES ('$password');";
	
	if(isset($_POST['registerbutton'])){
		echo "pressed";
		if($_POST['name'] && $_POST['email'] && $_POST['phone'] && $_POST['address'] && $_POST['password'] && $_POST['confirmpass']):
			$name = $_POST['name'];
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$address = $_POST['address'];
			$password = $_POST['password'];
			$confpass = $_POST['confirmpass'];
			$insertuser = "INSERT INTO user_data (name,email,phone,address,password) VALUES ('$name','$email','$phone','$address','$password');";
			if($password==$confpass){
				if($conn->query($insertuser)){
					header('Location: login.php');
				}else{
					echo "cannot insert";
				}
			}else{
				echo "password not matched";
			}
			
		else:
			echo "Fill the blanks";
		endif;
		
	}
?>
	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-60">
		<div class="container">
			<div class="row">
				
				<div style="text-align: center;" class="col md-12">
					<form action="" class="leave-comment" method="post">
						<h4 class="m-text26 p-b-36 p-t-15">
							<b>Register</b>
						</h4>


						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="name" placeholder="Full Name">
						</div>

						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="email" name="email" placeholder="Email Address">
						</div>

						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="phone" placeholder="Phone Number">
						</div>

						<textarea class="dis-block s-text7 size20 bo4 p-l-22 p-r-22 p-t-13 m-b-20" name="address" placeholder="Address"></textarea>

						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="password" name="password" placeholder="Password">
						</div>

						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="password" name="confirmpass" placeholder="Confirmation Password">
						</div>						
						

						<div class="w-size25">
							<!-- Button -->
							<input type="submit" name="registerbutton" value="REGISTER" class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>


	<?php require_once('include/footer.php')?>
	

</body>
</html>
