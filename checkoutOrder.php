<?php require_once('include/header.php'); ?>
<?php 
		$sesemail = $_SESSION['email'];
		$seldata = "SELECT * FROM user_data WHERE email = '$sesemail';";
		$user = $conn->query($seldata);
		$numrowdata = $user->num_rows;
?> 

<!-- Title Page -->
<?php 
	if(!isset($_SESSION['email'])):
		header("Location: login.php");
	else:
?>
		<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/heading-pages-021.jpg);">
			<h2 class="l-text2 t-center">
				INVOICE ORDER TAKE AWAY
			</h2>
		</section>

		<section class="bgwhite p-t-66 p-b-60">
			<div class="container">
				<div class="row">		
					<div style="text-align: center;" class="col md-12">
				<?php 
					while($namerow = $user->fetch_assoc()): 
				?>
							<h4 class="m-text26 p-b-36 p-t-15">
								<b>INVOICE ORDER TAKE AWAY</b>
							</h4>
							<div>
								<h3 class="normaltext"><?php echo $namerow['email'];?></h3>	
							</div>
							<h1>Shopping List</h1>
							<table class="table-shopping-cart" style="font-size: 24px;">
								<tr class="table-head">
									<th class="column-1"></th>
									<th class="column-2"></th>
									<th class="column-3"></th>
									<th class="column-4"></th>
								</tr>
				<?php
					endwhile;
					$selcart = "SELECT * FROM cart_data WHERE email = '$sesemail';";
					$cart = $conn->query($selcart);
					$numrowcart = $cart->num_rows;
					$totallprice = array();
					if($numrowcart==0):
					?>
						<h1>No Results</h1>
						go to <a href="product.php">here </a> to shop
					<?php
					else:
						while($cartrow = $cart->fetch_assoc()):
							array_push($totallprice,$cartrow['totalPrice']);
					?>							
								<tr class="table-row">
									<td class="column-1">
										<div class="cart-img-product b-rad-4 o-f-hidden">
											<img src="images/icons/shopping-cart.png" alt="IMG-PRODUCT">
										</div>
									</td>
									<td class="column-2" ><?php echo $cartrow['productName'];?></td>
									<td class="column-3" ><?php echo $cartrow['totalPrice'];?></td>
									<td class="column-4" ><?php echo $cartrow['quantity'];?></td>
								</tr>						
					<?php 
							deleteCart($cartrow['cartId']);
						endwhile;
					?>
								</table>
								<div class="flex-w flex-sb-m p-b-12">
									<span class="s-text18 w-size19 w-full-sm">
										Total Price:
									</span>

									<span class="m-text21 w-size20 w-full-sm">
										<?php echo array_sum($totallprice);?>
									</span>
								</div>
				<?php 
					endif;
				?>
					</div>				
				</div>
			</div>
		</section>
		
<?php 
	endif;			
?>	

	<?php require_once('include/footer.php')?>
	<?php  ?>
	
</body>
</html>
