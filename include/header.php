<?php 
	ob_start();
	session_start();
	include 'db-connect.php';
	include 'user-header.php';
?>
<html lang="en">
<head>
	<title>HAOZ-HAOZ | AUTHENTIC DRINKS & FOODS </title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/logo.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body  class="animsition">

	<!-- Header -->
	<header class="header1">
		<!-- Header desktop -->
		<div class="container-menu-header">
			

			<div class="wrap_header">
				<!-- Logo -->
				<a href="index.php" class="logo">
					<img src="images/icons/logo.png" alt="IMG-LOGO">
				</a>

				<!-- Menu -->
				<div class="wrap_menu">
					<nav class="menu">
						<ul class="main_menu">
							<li>
								<a href="index.php">Home</a>
							</li>

							<li>
								<a href="product.php">Shop</a>
							</li>

							<li>
								<a href="cart.php"><b>CART</b></a>
							</li>

							<li>
								<a href="about.php">About</a>
							</li>

							<li>
								<a href="contact.php">Contact</a>
							</li>
						</ul>
					</nav>
				</div>

				<!-- Header Icon -->
				<div class="header-icons">
					<?php						
						if(!isset($_SESSION['email'])):
					?>
							<a href="login.php" class="header-wrapicon1 dis-block">
								<img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
							</a>
					<?php 
						else:
					?>
							<div class="header-wrapicon2">
						<img src="images/icons/icon-header-01.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
						
						<!-- Header cart noti -->
						<div class="header-cart header-dropdown">
				

							<div class="header-cart-buttons">
								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="account.php" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										My Account
									</a>
								</div>

								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="logout-test.php" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Logout
									</a>
								</div>
							</div>
						</div>
					</div>
					<?php
						endif;

					?>

					<span class="linedivide1"></span>
					
					<div class="header-wrapicon2">
						<img src="images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">

						<!-- Header cart noti -->
						<?php 
							$totpricearr = array();
						?>
						<div class="header-cart header-dropdown">
							<ul class="header-cart-wrapitem">
								<?php 
									$useremail = $_SESSION['email'];
									$selcart = "SELECT * FROM cart_data WHERE email = '$useremail';";
									$qres = $conn->query($selcart);
									$numrowfood = $qres->num_rows;

									if($numrowfood==0):
								?>
									<h4 class="m-text14 p-b-7">
										No Results							
									</h4>
								<?php 
									else:
										while($headrow = $qres->fetch_assoc()):
									?>
											<li class="header-cart-item">
												<div class="header-cart-item-img">
													<img src="images/icons/shopping-cart.png" alt="IMG">
												</div>
												
												<div class="header-cart-item-txt">
														<?php 
															echo $headrow['productName'];
														?>
													
													<span class="header-cart-item-info">
														<?php 
															echo $headrow['totalPrice'];
															array_push($totpricearr,$headrow['totalPrice']);
														?>
													</span>
												</div>
											</li>
									<?php									
										endwhile;
								?>

									<?php
										endif;
									?>
								

							</ul>

							<div class="header-cart-total">
								<?php
									echo array_sum($totpricearr);
								?>
							</div>
						</div>
				</div>
				</div>
			</div>
		</div>
	</header>