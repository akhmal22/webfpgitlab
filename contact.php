<?php require_once('include/header.php'); ?>

	<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/heading-pages-021.jpg);">
		<h2 class="l-text2 t-center">
			Contact
		</h2>
	</section>

	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-60">
		<div class="container">
			<div class="row">
				<div class="col-md-6 p-b-30">
					<div id="map"></div>
						<style>
					      #map {
					        height: 500px;  
					        width: 100%; 
					       }
						</style>
						 <script>
						 	function initMap() {
							var lokasi = {lat: -6.22836055, lng: 106.78910494};
							var map = new google.maps.Map(
							document.getElementById('map'), {zoom: 15, center: lokasi});
							var marker = new google.maps.Marker({position: lokasi, map: map})
							}
						</script>
					<!-- Key Gmaps -->
						<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAi3njjxIeijiX2baqNT-Ky2IjVoBwQmN4&callback=initMap">
						</script>
				</div>

				<div class="col-md-6 p-b-30">
					
						<h3 class="m-text26 p-b-36 p-t-15">
							Find Us
						</h3>

						<div class="size15 m-b-50">
							<div class="gambar">
								<img src="images/map.png" alt="alamat">
								<h5>Kantin Atas, Universitas Pertamina. Jalan Teuku Nyak Arief, Simprug, Grogol Selatan, RT.7/RW.8, Grogol Selatan, Kebayoran Lama, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12220</h5>
							</div>
						</div>
						<br>
						<div class="size15 m-b-20">
							<div class="gambar">
								<img src="images/whatsapp.png" alt="alamat">
								<h5>087880086722</h5>
							</div>
						</div>

						<div class="size15 m-b-20">
							<div class="gambar">
								<img src="images/www.png" alt="alamat">
								<h5>https://haoz-haoz.com</h5>
							</div>
						</div>
				</div>
			</div>
		</div>
	</section>

<?php require_once('include/footer.php')?>