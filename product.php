<?php require_once('include/header.php'); ?>
<?php 

?> 

	<!-- Title Page -->
	<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(images/heading-pages-021.jpg);">
		<h2 class="l-text2 t-center">
			MENU CATEGORIES
		</h2>
	</section>


	<!-- Content page -->
	<div class="container">

  		<div class="jumbotron">
    		<h1>FOODS</h1>
			<a href="foods.php">Check out our finest foods here --></a>
  		</div>
		<div class="jumbotron">
    		<h1>DRINKS</h1>
    		<a href="drinks.php">Check out our finest drinks here --></a>
  		</div>
	</div>


<?php require_once('include/footer.php')?>

</body>
</html>
