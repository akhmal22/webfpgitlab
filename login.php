<?php require_once('include/header.php'); ?>
<?php 
	
	if(isset($_POST['signinbutton'])){
		echo "pressed";
		if($_POST['email'] && $_POST['password']){
			echo "setted";
			/*
			$sessionValid = $_SESSION['valid'];
			$sessionTimeout = $_SESSION['timeout'];
			$sessionEmail = $_SESSION['email'];
			 */
			$email = $_POST['email'];
			$passwd = $_POST['password'];
			$selectemail = "SELECT * FROM user_data WHERE email = '$email';";
			if($conn->query($selectemail)->num_rows > 0){
				$row = $conn->query($selectemail)->fetch_assoc();
				if($passwd == $row["password"]){
					$_SESSION['valid'] = true;
					$_SESSION['timeout'] = time();
					$_SESSION['email'] = $email;
					header('Location: index.php');
				}else{
					echo "incorrect password";
				}
				/*
				if($conn->num_rows($conn->query($selectemail)) > 0){
					echo "oke";
				}
				*/
			}else{
				//echo $conn->error();
				die("Register your email first!");
			}



		}else{
			echo "fill the blanks";
		}
	}
?>
	<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/heading-pages-021.jpg);">
		<h2 class="l-text2 t-center">
			Sign In
		</h2>
	</section>

	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-60">
		<div class="container">
			<div class="row">
				
				<div style="text-align: center;" class="col md-12">
					<form action="" class="leave-comment" method="post">
						<h4 class="m-text26 p-b-36 p-t-15">
							<b>Sign In</b>
						</h4>


						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="email" name="email" placeholder="Email Address">
						</div>

					
						<div class="bo4 of-hidden size15 m-b-20">
							<input class="sizefull s-text7 p-l-22 p-r-22" type="password" name="password" placeholder="Password">
						</div>

						<div class="w-size25">
							<!-- Button -->
							<input type="submit" class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4" name="signinbutton" value="SIGN IN">
						</div>
					</form>
					<br>
						<div class="w-size25">
							<!-- Button -->
							<a href="register.php">
							<input type="submit" class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4" value="REGISTER NOW">
							</a>
						</div>
				</div>
			</div>
		</div>
	</section>

	<?php require_once('include/footer.php')?>
	
</body>
</html>
