<?php require_once('include/header.php'); ?>
<?php 
	if(!isset($_SESSION['email'])):
		header("Location: login.php");
	else:
		$sesemail = $_SESSION['email'];
		$seldata = "SELECT * FROM user_data WHERE email = '$sesemail'";
		$user = $conn->query($seldata);
		$numrowdata = $user->num_rows;
?> 
		<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/heading-pages-021.jpg);">
			<h2 class="l-text2 t-center">
				Profile
			</h2>
		</section>

		<?php 
			while($namerow = $user->fetch_assoc()): 
		?>

				<section class="bgwhite p-t-66 p-b-38" id="test1" style="text-align: center; color: navy;">
					<div>
						<h2 class="normaltext"> <img src="images/icons/profilepicture.png" class="Profile"></h2>
					</div>
					<div>
						<h3 class="normaltext"><?php echo $namerow['name'];?></h3>	
					</div>
					<div>
						<h3 class="normaltext"><img src="images/icons/addres.png"><br><?php echo $namerow['address'];?></h3>	
					</div>
					<div>
						<h3 class="normaltext"><img src="images/icons/telpon.png"><br><?php echo $namerow['phone'];?></h3>	
					</div>
					<div>
						<h3 class="normaltext"><img src="images/icons/mail.png"><br><?php echo $namerow['email'];?></h3>	
					</div>
				</section>
		<?php 
			endwhile;
		?>	
<?php
	endif;
?>




	<?php require_once('include/footer.php')?>
</body>
</html>