
<?php require_once('include/header.php'); ?>
<?php 
	$selfood = "SELECT * FROM product_data WHERE categories = 'drinks';";
	$qres = $conn->query($selfood);
	$numrowfood = $qres->num_rows;
	/*
	if($numrowfood > 0){
		while($frow = $qres->fetch_assoc()){
			//echo $frow['prodName'];
		}
	}else{
		echo "no result";
	}
	 */

	$iter = 0;
	$arrfid = array();
	$arrfname = array();
	//while
?> 

	<!-- Title Page -->
	<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(images/heading-pages-021.jpg);">
		<h2 class="l-text2 t-center">
			FOODS
		</h2>
	</section>

	<section class="bgwhite p-t-55 p-b-65">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
					<div class="leftbar p-r-20 p-r-0-sm">
						<!--  -->
						<h4 class="m-text14 p-b-7">
							Categories
						</h4>

						<ul class="p-b-54">
							<li class="p-t-4">
								<a href="foods.php" class="s-text13">
									FOODS
								</a>
							</li>

							<li class="p-t-4">
								<a href="drinks.php" class="s-text13">
									DRINKS
								</a>
							</li>

						</ul>
					</div>
				</div>
				
				<?php 
					
					if ($numrowfood == 0):
						?>
						<h4 class="m-text14 p-b-7">
							No Results
							
						</h4>
						<?php
					else:
						?>
							<div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
								<!-- Product -->
								<div class="row">
								<?php
									while($namerow = $qres->fetch_assoc()):
										?>
											<div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
												<!-- Block2 -->
												<div class="block2">
													<div class="block2-img wrap-pic-w of-hidden pos-relative">
														<img src="images/<?php echo $namerow['prodImage'];?>" alt="IMG-PRODUCT">

														<div class="block2-overlay trans-0-4">
															<a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
																<i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
																<i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
															</a>

															<div class="block2-btn-addcart w-size1 trans-0-4">
																<!-- Button -->
																<form action="" method="get">
																	<button name="<?php echo $iter++;?>" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
																		Add to Cart
																	</button>
																</form>
															</div>
														</div>
													</div>

													<div class="block2-txt p-t-20">
														<a href="product-detail.html" class="block2-name dis-block s-text3 p-b-5">
															<?php
																array_push($arrfid,$namerow['prodId']);
																array_push($arrfname,$namerow['prodName']);
																echo $namerow['prodName'];
															?>
														</a>

														
														<span class="block2-price m-text6 p-r-5">
															<?php
																$arrfname[$iter] = $namerow['price']; 																
																echo $namerow['price'];
															?>
														</span>
													</div>
												</div>
											</div>
										<?php										
									endwhile;
								?>
								</div>
							</div>
							<?php 		
								if($_SESSION['email']){
									$iter = 0;
									$uemail = $_SESSION['email'];
									$iterstr = strval($iter);
									while($iter<$numrowfood){
										if(isset($_GET[$iter])){
											addToCart($uemail,$arrfid[$iter],1);
											//echo $arrfid[$iter];
											//echo "worked, probs";
										}
										$iter++;
									}																				
								}else{
									$iter = 0;
									//$uemail = $_SESSION['email'];
									while($iter<$numrowfood){
										//$iterstr = strval($iter);
										if(isset($_GET["$iter"])){											
											//addToCart($uemail,$arrfid[$iter],1);
											//echo $iter;
											header("Location: login.php");
										}									
										$iter++;
									}
								}
							?>					
						<?php
					endif;
				?>

				
						
			</div>
		</div>
	</section>



<?php require_once('include/footer.php')?>

</body>
</html>
