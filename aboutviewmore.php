	<section class="bgwhite p-t-66 p-b-38" id="test1">
		<div class="container">
			<div class="row">
				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Cleoputra Goldi A.
					</h3>

					<p class="p-b-28">
						<strong>Cleo</strong> merupakan salah seorang mahasiswa Program Studi Ilmu Komputer dengan NIM 105216023. Cleo lahir di Jakarta pada 22 Juli 1998. Dia berperan sebagai developer website ini di bagian <em>back-end</em>. Selain itu, dia juga merupakan seorang anak dari pendiri Haoz-Haoz ini.
					</p>
					<div class="bo13 p-l-29 m-l-9 p-b-10">
						<p class="p-b-11">
							I fear the day that technology will surpass our human interaction. The world will have a generation of idiots.
						</p>

						<span class="s-text7">
							- Cleo
						</span>
					</div>
				</div>

				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom">
						<img src="images/Foto- Cleo.jpg" alt="Cleoputra">
					</div>
				</div>

				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom">
						<img src="images/Foto- Kara.jpg" alt="Adikara">
					</div>
				</div>

				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Adikara Perkasa B. Tj
					</h3>

					<p class="p-b-28">
						<strong>Kara</strong> merupakan salah seorang mahasiswa Program Studi Ilmu Komputer dengan NIM 105216024. Kara lahir di Makassar pada 29 Desember 1998. Dia berperan sebagai developer website ini di bagian <em>front-end</em>.
					</p>
					<div class="bo13 p-l-29 m-l-9 p-b-10">
						<p class="p-b-11">
							Hidup Seperti <strong>Larry</strong>
						</p>

						<span class="s-text7">
							- Kara
						</span>
					</div>
				</div>

				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Nugi Gahara Yasa
					</h3>

					<p class="p-b-28">
						<strong>Nugi</strong> merupakan salah seorang mahasiswa Program Studi Ilmu Komputer dengan NIM 105216031. Nugi lahir di Garut pada 15 September 1997. Dia berperan sebagai developer website ini di bagian <em>front-end</em>.
					</p>
					<div class="bo13 p-l-29 m-l-9 p-b-10">
						<p class="p-b-11">
							Hidup itu <em>Bercemin</em>! Apapun yang dilakukan, itulah yang didapatkan.
						</p>

						<span class="s-text7">
							- Nugi
						</span>
					</div>
				</div>

				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom">
						<img src="images/Foto- Nugi.jpg" alt="Nugi">
					</div>
				</div>

				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom">
						<img src="images/Foto- Akhmal.jpg" alt="Akhmal">
					</div>
				</div>

				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Akhmal Rizkyanto
					</h3>

					<p class="p-b-28">
						<strong>Akhmal</strong> merupakan salah seorang mahasiswa Program Studi Ilmu Komputer dengan NIM 105216038. Akhmal lahir di Jakarta pada 12 September 1998. Dia berperan sebagai developer website ini di bagian <em>back-end</em>.
					</p>

					<div class="bo13 p-l-29 m-l-9 p-b-10">
						<p class="p-b-11">
							Stay in-line, Stay to focus!
						</p>

						<span class="s-text7">
							- Akhmal
						</span>
					</div>
				</div>
			</div>
		</div>
</section>