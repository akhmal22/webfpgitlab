<?php require_once('include/header.php'); ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>

		<script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>

	<script>
		$(document).ready(function() {
			$("#btn1").click(function(){
				$("#test2").load("aboutviewmore.php")
			}); 
		});
	</script>


</head>
<body>

	<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/heading-pages-021.jpg);">
		<h2 class="l-text2 t-center">
			About
		</h2>
	</section>

	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-38" id="test1">
		<div class="container">
			<div class="row">
				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom">
						<img src="images/icons/logo.png" alt="IMG-ABOUT">
					</div>
				</div>

				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Haoz-Haoz Story
					</h3>

					<p class="p-b-28">
						Haoz-Haoz merupakan salah satu warung jajanan di kantin Universitas Pertamina yang menjual berbagai jenis minuman panas dan dingin serta cemilan seperti cilok dan seblak. Haoz-Haoz berdiri sejak tahun "...." dengan pendirinya yakni "....". Haoz-Haoz hanya membuka kedai di kantin Universitas Pertamina dan belum memiliki cabang dimana pun.
					</p>

					<div class="bo13 p-l-29 m-l-9 p-b-10">
						<p class="p-b-11">
							Good Eat, Good Mood
						</p>

						<span class="s-text7">
							- Haoz-Haoz
						</span>
					</div>

					</div>

					<div id="test2"></div>
				

			</div>
		</div>
		
			

	</section>
			<br/>
				<div class="w-size25">

					<button class="button1" id="btn1">
						Show More
					</button>
				</div>
			<br/>

</body>
</html>


	<?php require_once('include/footer.php')?>
	
</body>
</html>
